package com.tpr.modelmuseum.model;

public enum UserRole {
    ADMIN,
    NORMAL
}
