package com.tpr.modelmuseum.model;

public enum MediaFileType {

    IMAGE,
    VIDEO,
    AUDIO,
    TEXT;

    public String toString(MediaFileType mediaFileType) {
        switch (mediaFileType) {
            case IMAGE: return "Image";
            case VIDEO: return "Video";
            case AUDIO: return "Audio";
            case TEXT: return "Text";
            default: return "Unknown";
        }
    }
}
