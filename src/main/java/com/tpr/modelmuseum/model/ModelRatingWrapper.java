package com.tpr.modelmuseum.model;

import com.tpr.modelmuseum.model.entities.Model;
import com.tpr.modelmuseum.model.entities.User;

import java.math.BigDecimal;

public class ModelRatingWrapper {

    private Model model;
    private BigDecimal averageRating;

    public ModelRatingWrapper(Model model, BigDecimal averageRating) {
        this.model = model;
        this.averageRating = averageRating;
    }

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    public BigDecimal getAverageRating() {
        return averageRating;
    }

    public void setAverageRating(BigDecimal averageRating) {
        this.averageRating = averageRating;
    }
}
