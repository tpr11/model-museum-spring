package com.tpr.modelmuseum.model;

public enum ModelFileType {

    TENSORFLOW,
    PYTORCH,
    CAFFE,
    KERAS,
    OTHERS;

    public String toString(ModelFileType modelFileType) {
        switch (modelFileType) {
            case TENSORFLOW: return "Tensorflow";
            case PYTORCH: return "PyTorch";
            case CAFFE: return "Caffe";
            case KERAS:return "Keras";
            case OTHERS: return "Others";
            default: return "Unknown";
        }
    }

}
