package com.tpr.modelmuseum.model;

public enum ModelType {

    COMPUTER_VISION,
    NATURAL_LANGUAGE_PROCESSING,
    REINFORCEMENT_LEARNING,
    OTHERS;

    public String toString(ModelType modelType) {
        switch (modelType) {
            case COMPUTER_VISION: return "Computer Vision";
            case NATURAL_LANGUAGE_PROCESSING: return "Natural Language Processing";
            case REINFORCEMENT_LEARNING: return "Reinforcement Learning";
            case OTHERS: return "OTHERS";
            default: return "UNKNOWN";
        }
    }
}
