package com.tpr.modelmuseum.model.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "model_ratings")
@EntityListeners(AuditingEntityListener.class)
public class ModelRating {

    @Id
    @Column(name = "model_ratings_id")
    @JsonIgnore
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne(targetEntity = User.class)
    @NotNull
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private User user;

    @ManyToOne(targetEntity = Model.class)
    @NotNull
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Model model;

    @Column(nullable = false)
    private Integer rating;

    @Column(nullable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Date createdAt;

    @Column(nullable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @JsonIgnore
    @LastModifiedDate
    private Date updatedAt;

    public ModelRating() {}

    public ModelRating(User user, Model model, Integer rating) {
        this.user = user;
        this.model = model;
        this.rating = rating;
    }

    public Long getId() {
        return id;
    }

    public String getUser() {
        return user.getUsername();
    }

    public Long getModel() {
        return model.getId();
    }

    public Integer getRating() {
        return rating;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ModelRating that = (ModelRating) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
