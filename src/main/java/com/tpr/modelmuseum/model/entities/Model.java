package com.tpr.modelmuseum.model.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tpr.modelmuseum.model.ModelType;
import jdk.nashorn.internal.ir.ObjectNode;
import org.hibernate.annotations.Formula;
import org.json.JSONObject;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "models")
@EntityListeners(AuditingEntityListener.class)
public class Model {

    @Id
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false, length = 128)
    private String name;

    @Column(nullable = false)
    private ModelType modelType;

    @Column(length = 65536)
    private String description;

    @OneToMany(targetEntity = MediaFile.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @NotNull
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @OrderColumn(name="index")
    private List<MediaFile> mediaFiles;

    @OneToMany(targetEntity = ModelFile.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @NotNull
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @OrderColumn(name="index")
    private List<ModelFile> modelFiles;

    @Formula(value = "(SELECT COALESCE(AVG(mr.rating), 0) " +
            "FROM models m " +
            "LEFT JOIN model_ratings mr ON m.id = mr.model_id " +
            "WHERE m.id = id)")
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Double averageRating;

    @Formula(value = "(SELECT COUNT(mr.rating) " +
            "FROM model_ratings mr " +
            "WHERE mr.model_id = id)")
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Long ratings;

    @ManyToOne(targetEntity = User.class)
    @NotNull
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private User user;

    @Column(nullable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @CreatedDate
    private Date createdAt;

    @Column(nullable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @JsonIgnore
    @LastModifiedDate
    private Date updatedAt;

    public Model() {}

    public Model(String jsonString) {
        JSONObject json = new JSONObject(jsonString);
        this.name = json.has("name") ? (String) json.get("name") : null;
        this.modelType = json.has("modelType") ? ModelType.valueOf((String) json.get("modelType")) : null;
        this.description = json.has("description") ? (String) json.get("description") : null;
    }

    public Model(String jsonString, User user) {
        this(jsonString);
        this.user = user;
    }

    public Model(String name, ModelType modelType, String description, User user) {
        this.name = name;
        this.modelType = modelType;
        this.description = description;
        this.user = user;
    }

    public Model(Model model, Double averageRating, Long ratings) {
        this.id = model.id;
        this.name = model.name;
        this.modelType = model.modelType;
        this.description = model.description;
        this.modelFiles = model.modelFiles;
        this.mediaFiles = model.mediaFiles;
        this.user = model.user;
        this.createdAt = model.createdAt;
        this.updatedAt = model.updatedAt;
        this.averageRating = averageRating;
        this.ratings = ratings;
    }

    public Model(String name, ModelType modelType, String description, User user, @NotNull List<MediaFile> mediaFiles, @NotNull List<ModelFile> modelFiles) {
        this(name, modelType, description, user);
        this.mediaFiles = mediaFiles;
        this.modelFiles = modelFiles;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ModelType getModelType() {
        return modelType;
    }

    public void setModelType(ModelType modelType) {
        this.modelType = modelType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<MediaFile> getMediaFiles() {
        return mediaFiles;
    }

    public void setMediaFiles(List<MediaFile> mediaFiles) {
        this.mediaFiles = mediaFiles;
    }

    public List<ModelFile> getModelFiles() {
        return modelFiles;
    }

    public void setModelFiles(List<ModelFile> modelFiles) {
        this.modelFiles = modelFiles;
    }

    @Transient
    public Double getAverageRating() {
        return averageRating;
    }

    @Transient
    public Long getRatings() { return ratings; }

    public String getUser() {
        return user.getUsername();
    }

    @JsonIgnore
    public User getUserFull() { return user; }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Model model = (Model) o;
        return Objects.equals(id, model.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
