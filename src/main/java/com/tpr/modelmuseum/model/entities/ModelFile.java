package com.tpr.modelmuseum.model.entities;

import com.tpr.modelmuseum.model.MediaFileType;
import com.tpr.modelmuseum.model.ModelFileType;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "model_files")
@EntityListeners(AuditingEntityListener.class)
public class ModelFile extends LocalFile {

    @Column(nullable = false)
    private ModelFileType type;

    public ModelFile() {}

    public ModelFile(Model model, String filename, ModelFileType type) {
        super(model, filename);
        this.type = type;
    }

    public ModelFileType getType() {
        return type;
    }

    public void setType(ModelFileType type) {
        this.type = type;
    }
}
