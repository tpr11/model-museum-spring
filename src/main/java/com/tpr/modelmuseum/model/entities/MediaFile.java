package com.tpr.modelmuseum.model.entities;

import com.tpr.modelmuseum.model.MediaFileType;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Table(name = "media_files")
@EntityListeners(AuditingEntityListener.class)
public class MediaFile extends LocalFile {

    @Column(nullable = false)
    private MediaFileType type;

    public MediaFile() {}

    public MediaFile(Model model, String filename, MediaFileType type) {
        super(model, filename);
        this.type = type;
    }

    public MediaFileType getType() {
        return type;
    }

    public void setType(MediaFileType type) {
        this.type = type;
    }
}
