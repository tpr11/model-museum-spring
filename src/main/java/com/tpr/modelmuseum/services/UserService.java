package com.tpr.modelmuseum.services;

import com.tpr.modelmuseum.exceptions.NotFoundException;
import com.tpr.modelmuseum.model.entities.User;

import java.util.UUID;

public interface UserService {
    User getUser(UUID id) throws NotFoundException;
    User getUser(String username) throws NotFoundException;
    void createUser(User user);
}
