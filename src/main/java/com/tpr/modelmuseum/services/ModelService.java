package com.tpr.modelmuseum.services;

import com.tpr.modelmuseum.model.MediaFileType;
import com.tpr.modelmuseum.model.ModelFileType;
import com.tpr.modelmuseum.model.ModelType;
import com.tpr.modelmuseum.model.entities.*;
import org.springframework.core.io.Resource;
import org.springframework.data.util.Pair;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

public interface ModelService {

    List<Model> getModels(int page, int limit, String sort, boolean ascending, ModelType modelType);
    Model getModel(Long modelId);
    ModelRating getModelRating(UUID userId, Long modelId);

    abstract LocalFile getLocalFile(Long fileId) throws Exception;
    ModelFile getModelFile(Long fileId);
    MediaFile getMediaFile(Long fileId);

    void createModel(UUID userId, Model model, MultipartFile[] modelFiles, ModelFileType[] modelFileTypes, MultipartFile[] mediaFiles) throws IOException;

    void updateModel(UUID userId, Long modelId, Model newModel);
    void renameFile(UUID userId, Long fileId, String newName);
    void changeFileType(UUID userId, Long fileId, ModelFileType modelFileType);
    void changeFileType(UUID userId, Long fileId, MediaFileType mediaFileType);
    void addModelFiles(UUID userId, Long modelId, MultipartFile[] modelFiles, ModelFileType[] modelFileTypes) throws IOException;
    void addMediaFiles(UUID userId, Long modelId, MultipartFile[] mediaFiles) throws IOException;

    Pair<Resource, LocalFile> getFile(Long fileId) throws Exception;

    void deleteModel(UUID userId, Long modelId);
    @Transactional
    void deleteFile(UUID userId, Long fileId);

    void rateModel(UUID userId, Long modelId, Integer rating);
    void unrateModel(UUID userId, Long modelId);
}
