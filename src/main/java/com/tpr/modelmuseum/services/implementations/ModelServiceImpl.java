package com.tpr.modelmuseum.services.implementations;

import com.tpr.modelmuseum.exceptions.InvalidDataException;
import com.tpr.modelmuseum.exceptions.NotFoundException;
import com.tpr.modelmuseum.exceptions.ForbiddenException;
import com.tpr.modelmuseum.model.*;
import com.tpr.modelmuseum.model.entities.*;
import com.tpr.modelmuseum.repositories.*;
import com.tpr.modelmuseum.services.ModelService;
import com.tpr.modelmuseum.services.UserService;
import com.tpr.modelmuseum.util.Validation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
public class ModelServiceImpl implements ModelService {

    @Value("${file.local-file-dir}")
    private String localFileDir;

    @Autowired
    private UserService userService;

    @Autowired
    private ModelRepository modelRepository;

    @Autowired
    private ModelRatingRepository modelRatingRepository;

    @Autowired
    private LocalFileRepository localFileRepository;

    @Autowired
    private ModelFileRepository modelFileRepository;

    @Autowired
    private MediaFileRepository mediaFileRepository;

    @Override
    public List<Model> getModels(int page, int limit, String sort, boolean ascending, ModelType modelType) {
        if (page < 0) {
            return new ArrayList<>();
        }
        else if (modelType == null) {
            return modelRepository.findAllWithRatings(PageRequest.of(page, limit, new Sort(ascending ? Sort.Direction.DESC : Sort.Direction.ASC, sort))).getContent();
        }
        else {
            return modelRepository.findAllByModelTypeWithRatings(PageRequest.of(page, limit, new Sort(ascending ? Sort.Direction.DESC : Sort.Direction.ASC, sort)), modelType).getContent();
        }
    }

    @Override
    public void createModel(UUID userId, final Model model, MultipartFile[] modelFiles, ModelFileType[] modelFileTypes, MultipartFile[] mediaFiles) throws IOException {

        User user = userService.getUser(userId);
        model.setUser(user);

        //TODO
        MediaFileType[] mediaFileTypes = new MediaFileType[mediaFiles.length];
        IntStream.range(0, mediaFileTypes.length).forEach(i -> mediaFileTypes[i] = MediaFileType.IMAGE);

        List<ModelFile> modelFileList = IntStream
                .range(0, modelFiles.length)
                .mapToObj(i -> new ModelFile(model, modelFiles[i].getOriginalFilename(), modelFileTypes[i]))
                .collect(Collectors.toList());
        model.setModelFiles(modelFileList);

        List<MediaFile> mediaFileList = IntStream
                .range(0, mediaFiles.length)
                .mapToObj(i -> new MediaFile(model, mediaFiles[i].getOriginalFilename(), mediaFileTypes[i]))
                .collect(Collectors.toList());
        model.setMediaFiles(mediaFileList);

        if (!Validation.validModel(model)) {
            throw new InvalidDataException("Model does not meet guidelines!");
        }

        Model newModel = modelRepository.save(model);
        modelFileList = newModel.getModelFiles();
        mediaFileList = newModel.getMediaFiles();

        for (int i = 0; i < modelFiles.length && i < modelFileList.size(); i++) {
            MultipartFile modelFile = modelFiles[i];
            try(FileOutputStream out = new FileOutputStream(String.format("%s/%d", localFileDir, modelFileList.get(i).getId()))) {
                out.write(modelFile.getBytes());
            }
        }

        for (int i = 0; i < mediaFiles.length && i < mediaFileList.size(); i++) {
            MultipartFile mediaFile = mediaFiles[i];
            try(FileOutputStream out = new FileOutputStream(String.format("%s/%d", localFileDir, mediaFileList.get(i).getId()))) {
                out.write(mediaFile.getBytes());
            }
        }
    }

    @Override
    public Model getModel(Long modelId) {
        Optional<Model> modelOptional = modelRepository.findById(modelId);
        if (!modelOptional.isPresent()) {
            throw new NotFoundException("Model not found.");
        }
        return modelOptional.get();
    }

    @Override
    public ModelRating getModelRating(UUID userId, Long modelId) {
        Optional<ModelRating> modelRatingOptional = modelRatingRepository.findByUserIdAndModelId(userId, modelId);
        if (!modelRatingOptional.isPresent()) {
            throw new NotFoundException("Model has not been rated by user.");
        }
        return modelRatingOptional.get();
    }

    @Override
    public LocalFile getLocalFile(Long fileId) {
        Optional<LocalFile> localFileOptional = localFileRepository.findById(fileId);
        if (!localFileOptional.isPresent()) {
            throw new NotFoundException("File not found.");
        }
        return localFileOptional.get();
    }

    @Override
    public ModelFile getModelFile(Long fileId) {
        Optional<LocalFile> modelFileOptional = modelFileRepository.findById(fileId);
        if (!modelFileOptional.isPresent()) {
            throw new NotFoundException("Model file not found.");
        }
        return (ModelFile) modelFileOptional.get();
    }

    @Override
    public MediaFile getMediaFile(Long fileId) {
        Optional<LocalFile> mediaFileOptional = mediaFileRepository.findById(fileId);
        if (!mediaFileOptional.isPresent()) {
            throw new NotFoundException("Media file not found.");
        }
        return (MediaFile) mediaFileOptional.get();
    }

    private void assertCanChange(User user, Model model) {
        if (user.getUserRole() != UserRole.ADMIN && !model.getUserFull().equals(user)) {
            throw new ForbiddenException("User does not have permission to delete model.");
        }
    }

    @Override
    public Pair<Resource, LocalFile> getFile(Long fileId) throws NotFoundException, MalformedURLException {
        LocalFile localFile = getLocalFile(fileId);
        String filename = String.format("%s/%s", localFileDir, localFile.getFilepath());
        Path filePath = Paths
                .get(filename)
                .toAbsolutePath()
                .normalize();
        Resource resource = new UrlResource(filePath.toUri());
        if(!resource.exists()) {
            throw new NotFoundException("File not found " + fileId.toString());
        }
        return Pair.of(resource, localFile);
    }

    @Override
    public void deleteModel(UUID userId, Long modelId) {
        User user = userService.getUser(userId);
        Model model = getModel(modelId);
        assertCanChange(user, model);
        modelRepository.deleteById(modelId);
    }

    private void deleteModelFile(Long modelFileId) {
        Integer index = modelFileRepository.getAssociativeTableIndex(modelFileId);
        modelFileRepository.deleteAssociativeTableKey(modelFileId);
        modelFileRepository.updateAssociativeTableIndexes(modelFileId, index);
        modelFileRepository.deleteMediaFileKey(modelFileId);
        modelFileRepository.deleteLocalFilesKey(modelFileId);
    }

    private void deleteMediaFile(Long mediaFileId) {
        Integer index = mediaFileRepository.getAssociativeTableIndex(mediaFileId);
        mediaFileRepository.deleteAssociativeTableKey(mediaFileId);
        mediaFileRepository.updateAssociativeTableIndexes(mediaFileId, index);
        mediaFileRepository.deleteMediaFileKey(mediaFileId);
        mediaFileRepository.deleteLocalFilesKey(mediaFileId);
    }

    @Transactional
    public void deleteFile(UUID userId, Long fileId) {
        User user = userService.getUser(userId);
        LocalFile localFile = getLocalFile(fileId);
        assertCanChange(user, localFile.getModelFull());
        if (localFile instanceof ModelFile) {
            deleteModelFile(fileId);
        }
        else if (localFile instanceof MediaFile) {
            deleteMediaFile(fileId);
        }
        new File(localFileDir + "/" + localFile.getFilepath()).delete();
    }

    @Override
    public void updateModel(UUID userId, Long modelId, Model newModel) {
        User user = userService.getUser(userId);
        Model model = getModel(modelId);
        assertCanChange(user, model);
        model.setName(newModel.getName() == null ? model.getName() : newModel.getName());
        model.setModelType(newModel.getModelType() == null ? model.getModelType() : newModel.getModelType());
        model.setDescription(newModel.getDescription() == null ? model.getDescription() : newModel.getDescription());
        if (!Validation.validModel(model)) {
            throw new InvalidDataException("New model does not meet guidelines.");
        }
        modelRepository.save(model);
    }

    @Override
    public void renameFile(UUID userId, Long fileId, String newName) {
        User user = userService.getUser(userId);
        LocalFile localFile = getLocalFile(fileId);
        assertCanChange(user, localFile.getModelFull());
        localFile.setFilename(newName);
        localFileRepository.save(localFile);
    }

    @Override
    public void changeFileType(UUID userId, Long fileId, ModelFileType modelFileType) {
        User user = userService.getUser(userId);
        ModelFile localFile = getModelFile(fileId);
        assertCanChange(user, localFile.getModelFull());
        localFile.setType(modelFileType);
    }

    @Override
    public void changeFileType(UUID userId, Long fileId, MediaFileType mediaFileType) {
        User user = userService.getUser(userId);
        MediaFile localFile = getMediaFile(fileId);
        assertCanChange(user, localFile.getModelFull());
        localFile.setType(mediaFileType);
    }

    @Override
    public void addModelFiles(UUID userId, Long modelId, MultipartFile[] modelFiles, ModelFileType[] modelFileTypes) throws IOException {

        User user = userService.getUser(userId);
        Model model = getModel(modelId);
        assertCanChange(user, model);

        int oldSize = model.getModelFiles().size();
        model.getModelFiles().addAll(IntStream
                .range(0, modelFiles.length)
                .mapToObj(i -> new ModelFile(model, modelFiles[i].getOriginalFilename(), modelFileTypes[i]))
                .collect(Collectors.toList()));

        Model newModel = modelRepository.save(model);
        List<ModelFile> modelFileList = newModel.getModelFiles();

        for (int i = 0; i < modelFiles.length && i + oldSize < modelFileList.size(); i++) {
            MultipartFile modelFile = modelFiles[i];
            try(FileOutputStream out = new FileOutputStream(String.format("%s/%d", localFileDir, modelFileList.get(i + oldSize).getId()))) {
                out.write(modelFile.getBytes());
            }
        }
    }

    @Override
    public void addMediaFiles(UUID userId, Long modelId, MultipartFile[] mediaFiles) throws IOException {
        
        User user = userService.getUser(userId);
        Model model = getModel(modelId);
        assertCanChange(user, model);

        //TODO
        MediaFileType[] mediaFileTypes = new MediaFileType[mediaFiles.length];
        IntStream.range(0, mediaFileTypes.length).forEach(i -> mediaFileTypes[i] = MediaFileType.IMAGE);

        int oldSize = model.getMediaFiles().size();
        model.getMediaFiles().addAll(IntStream
                .range(0, mediaFiles.length)
                .mapToObj(i -> new MediaFile(model, mediaFiles[i].getOriginalFilename(), mediaFileTypes[i]))
                .collect(Collectors.toList()));

        Model newModel = modelRepository.save(model);
        List<MediaFile> mediaFileList = newModel.getMediaFiles();

        for (int i = 0; i < mediaFiles.length && i + oldSize < mediaFileList.size(); i++) {
            MultipartFile mediaFile = mediaFiles[i];
            try(FileOutputStream out = new FileOutputStream(String.format("%s/%d", localFileDir, mediaFileList.get(i + oldSize).getId()))) {
                out.write(mediaFile.getBytes());
            }
        }
    }

    @Override
    public void rateModel(UUID userId, Long modelId, Integer rating) {
        User user = userService.getUser(userId);
        Model model = getModel(modelId);
        ModelRating modelRating = new ModelRating(user, model, rating);
        Optional<ModelRating> modelRatingOptional = modelRatingRepository.findByUserIdAndModelId(userId, modelId);
        if (modelRatingOptional.isPresent()) {
            modelRating = modelRatingOptional.get();
            modelRating.setRating(rating);
        }
        modelRatingRepository.save(modelRating);
    }

    @Override
    public void unrateModel(UUID userId, Long modelId) {
        modelRatingRepository.deleteById(getModelRating(userId, modelId).getId());
    }
}
