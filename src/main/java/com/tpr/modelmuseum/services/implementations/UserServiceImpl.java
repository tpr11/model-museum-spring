package com.tpr.modelmuseum.services.implementations;

import com.tpr.modelmuseum.exceptions.NotFoundException;
import com.tpr.modelmuseum.model.entities.User;
import com.tpr.modelmuseum.repositories.UserRepository;
import com.tpr.modelmuseum.services.UserService;
import com.tpr.modelmuseum.util.Validation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public User getUser(UUID id) throws NotFoundException {
        Optional<User> userOptional = userRepository.findById(id);
        if (!userOptional.isPresent()) {
            throw new NotFoundException("User does not exist!");
        }
        return userOptional.get();
    }

    @Override
    public User getUser(String username) {
        Optional<User> userOptional = userRepository.findByUsername(username);
        if (!userOptional.isPresent()) {
            throw new NotFoundException("User does not exist!");
        }
        return userOptional.get();
    }

    @Override
    public void createUser(User user) {
        Validation.assertValidUser(user);
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        userRepository.save(user);
    }
}
