package com.tpr.modelmuseum.util;

import org.springframework.stereotype.Component;

@Component
public abstract class Config {
    public static final int JWT_LIFETIME = 604_800_000;
    public static final String JWT_SECRET = "hUPu0zkpDmzFG3nUSMm0D0WN5ERTnm6ePc2jD1aqZU6bv6GJ5BI5x49VkNxmpel6";
}
