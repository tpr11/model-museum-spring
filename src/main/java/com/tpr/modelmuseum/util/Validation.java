package com.tpr.modelmuseum.util;

import com.tpr.modelmuseum.exceptions.InvalidDataException;
import com.tpr.modelmuseum.model.entities.Model;
import com.tpr.modelmuseum.model.entities.ModelRating;
import com.tpr.modelmuseum.model.entities.User;
import com.tpr.modelmuseum.repositories.UserRepository;
import io.jsonwebtoken.lang.Collections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class Validation {

    private static UserRepository userRepository;

    @Autowired
    public Validation(UserRepository userRepository) {
        Validation.userRepository = userRepository;
    }

    public static boolean validUser(User user) {
        return  user.getUsername() != null &&
                user.getUsername().matches("^[a-z0-9_-]{3,32}$") &&
                user.getEmail() != null &&
                user.getEmail().matches("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$") &&
                user.getPassword() != null &&
                user.getPassword().matches("^.{6,128}$") &&
                user.getUserRole() != null;
    }

    public static void assertValidUser(User user) {
        List<String> errors = new ArrayList<>();
        if (user.getUsername() == null) {
            errors.add("Username is missing.");
        }
        else {
            if (!user.getUsername().matches("^[a-z0-9_-]{3,32}$")) {
                errors.add("Username must be between 3 and 32 characters long, and must only contain alphanumeric characters and '-' or '_'.");
            }
            if (userRepository.findByUsername(user.getUsername()).isPresent()) {
                errors.add("Username already exists.");
            }
        }
        if (user.getEmail() == null) {
            errors.add("Email is missing.");
        }
        else {
            if (!user.getEmail().matches("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")) {
                errors.add("Email is not a valid email address.");
            }
            if (userRepository.findByEmail(user.getEmail()).isPresent()) {
                errors.add("Email already exists.");
            }
        }
        if (user.getPassword() == null) {
            errors.add("Password is missing.");
        }
        else if (!user.getPassword().matches("^.{6,128}$")) {
            errors.add("Password must be between 4 and 128 characters long.");
        }
        if (errors.size() > 0) {
            throw new InvalidDataException(errors);
        }
    }

    public static boolean validModel(Model model) {
        return  model.getName() != null &&
                model.getName().matches("^.{1,128}$") &&
                model.getModelType() != null &&
                (model.getDescription() == null ||
                        model.getDescription().length() <= 65536) &&
                model.getModelFiles() != null &&
                model.getModelFiles().size() > 0;
    }

    public static boolean validModelRating(ModelRating modelRating) {
        return  modelRating.getRating() != null &&
                modelRating.getRating() > 0 &&
                modelRating.getRating() <= 10;
    }

}
