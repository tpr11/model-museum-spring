package com.tpr.modelmuseum.controllers;

import com.tpr.modelmuseum.exceptions.InvalidDataException;
import com.tpr.modelmuseum.model.entities.User;
import com.tpr.modelmuseum.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api")
public class AuthenticationController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ResponseEntity register(@RequestBody User user) {
        userService.createUser(user);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/check-token", method = RequestMethod.POST)
    public ResponseEntity checkToken() {
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

}
