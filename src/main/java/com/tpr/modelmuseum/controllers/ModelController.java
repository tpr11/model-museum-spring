package com.tpr.modelmuseum.controllers;

import com.tpr.modelmuseum.exceptions.InvalidDataException;
import com.tpr.modelmuseum.model.MediaFileType;
import com.tpr.modelmuseum.model.ModelFileType;
import com.tpr.modelmuseum.model.ModelRatingWrapper;
import com.tpr.modelmuseum.model.ModelType;
import com.tpr.modelmuseum.model.entities.*;
import com.tpr.modelmuseum.repositories.ModelRepository;
import com.tpr.modelmuseum.services.ModelService;
import jdk.nashorn.internal.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.data.util.Pair;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.Principal;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

@RestController
@RequestMapping("/api")
public class ModelController {

    @Autowired
    private ModelService modelService;

    @RequestMapping(value = { "/public/models", "/public/models/{modelType}" }, method = RequestMethod.GET)
    public ResponseEntity<List<Model>> getModels(@PathVariable("modelType") Optional<ModelType> modelType,
                                                              @RequestParam(value = "page", required = false, defaultValue = "0") Integer page,
                                                              @RequestParam(value = "limit", required = false, defaultValue = "20") Integer limit,
                                                              @RequestParam(value = "sort", required = false, defaultValue = "DATE") String sort,
                                                              @RequestParam(value = "ascending", required = false, defaultValue = "false") Boolean ascending) {
        limit = limit >= 100 ? 100 : limit;
        sort = (sort.equals("NAME") ?
                "name" :
                (sort.equals("RATING") ?
                        "averageRating" :
                        (sort.equals("RATINGS") ?
                                "ratings" :
                                "createdAt")));
        return new ResponseEntity<>(modelService.getModels(page, limit, sort, ascending, modelType.orElse(null)), HttpStatus.OK);
    }

    @RequestMapping(value = "/public/model/{id}", method = RequestMethod.GET)
    public ResponseEntity<Model> getModel(@PathVariable(value = "id") Long modelId) {
        return new ResponseEntity<>(modelService.getModel(modelId), HttpStatus.OK);
    }

    @RequestMapping(value = "/model/new", method = RequestMethod.POST)
    public ResponseEntity createModel(@RequestParam("model") String modelJSON,
                                      @RequestParam("modelFiles") MultipartFile[] modelFiles,
                                      @RequestParam("modelFileTypes") ModelFileType[] modelFileTypes,
                                      @RequestParam("mediaFiles") MultipartFile[] mediaFiles,
                                      Principal principal) {
        if (modelFiles == null || modelFiles.length == 0) {
            throw new InvalidDataException("Model files missing. Each submission must include at least one pre-trained model file.");
        }
        else {
            List<String> invalidData = new ArrayList<>();
            if (modelFiles.length != modelFileTypes.length) {
                invalidData.add("Number of model file types different from number of model files. The two must correspond.");
            }
            if (modelFiles.length > 10) {
                invalidData.add("Maximum 10 model files.");
            }
            if (!invalidData.isEmpty()) {
                throw new InvalidDataException(invalidData);
            }
        }
        try {
            modelService.createModel((UUID) ((UsernamePasswordAuthenticationToken) principal).getPrincipal(), new Model(modelJSON), modelFiles, modelFileTypes, mediaFiles);
        } catch (IOException e) {
            e.printStackTrace();
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/model/{id}/delete", method = RequestMethod.POST)
    public ResponseEntity deleteModel(@PathVariable(value = "id") Long modelId,
                                      Principal principal) {
        modelService.deleteModel((UUID) ((UsernamePasswordAuthenticationToken) principal).getPrincipal(), modelId);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/model/{id}/update", method = RequestMethod.POST)
    public ResponseEntity updateModel(@PathVariable(value = "id") Long modelId,
                                      @RequestBody Model model,
                                      Principal principal) {
        modelService.updateModel((UUID) ((UsernamePasswordAuthenticationToken) principal).getPrincipal(), modelId, model);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/model/{id}/rate", method = RequestMethod.POST)
    public ResponseEntity rateModel(@PathVariable(value = "id") Long modelId,
                                    @RequestParam("rating") Integer rating,
                                    Principal principal) {
        modelService.rateModel((UUID) ((UsernamePasswordAuthenticationToken) principal).getPrincipal(), modelId, rating);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/model/{id}/unrate", method = RequestMethod.POST)
    public ResponseEntity unrateModel(@PathVariable(value = "id") Long modelId,
                                      Principal principal) {
        modelService.unrateModel((UUID) ((UsernamePasswordAuthenticationToken) principal).getPrincipal(), modelId);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/model/{id}/model-files/new", method = RequestMethod.POST)
    public ResponseEntity addModelFiles(@PathVariable(value = "id") Long modelId,
                                        @RequestParam("modelFiles") MultipartFile[] modelFiles,
                                        @RequestParam("modelFileTypes") ModelFileType[] modelFileTypes,
                                        Principal principal) {
        if (modelFiles == null || modelFiles.length < 1 || modelFileTypes == null || modelFiles.length != modelFileTypes.length) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        try {
            modelService.addModelFiles((UUID) ((UsernamePasswordAuthenticationToken) principal).getPrincipal(), modelId, modelFiles, modelFileTypes);
        }
        catch (IOException e) {
            e.printStackTrace();
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/model/{id}/media-files/new", method = RequestMethod.POST)
    public ResponseEntity addMediaFiles(@PathVariable(value = "id") Long modelId,
                                        @RequestParam("mediaFiles") MultipartFile[] mediaFiles,
                                        Principal principal) {
        if (mediaFiles == null || mediaFiles.length < 1) {
            throw new InvalidDataException("No files included.");
        }
        try {
            modelService.addMediaFiles((UUID) ((UsernamePasswordAuthenticationToken) principal).getPrincipal(), modelId, mediaFiles);
        }
        catch (IOException e) {
            e.printStackTrace();
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/public/file/{id}", method = RequestMethod.GET)
    public ResponseEntity<Resource> getFile(@PathVariable(value = "id") Long fileId) throws Exception {

        Pair<Resource, LocalFile> file = modelService.getFile(fileId);
        Resource actualFile = file.getFirst();
        LocalFile localFile = file.getSecond();

        return ResponseEntity
                .ok()
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .cacheControl(CacheControl.maxAge(1, TimeUnit.HOURS))
                .header("Content-Disposition", "attachment; filename=\"" + localFile.getFilename() + "\"")
                .body(actualFile);
    }

    @RequestMapping(value = "/file/{id}/rename", method = RequestMethod.POST)
    public ResponseEntity renameFile(@PathVariable(value = "id") Long fileId,
                                     @RequestParam(value = "name") String name,
                                     Principal principal) {
        modelService.renameFile((UUID) ((UsernamePasswordAuthenticationToken) principal).getPrincipal(), fileId, name);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/file/{id}/delete", method = RequestMethod.POST)
    public ResponseEntity deleteFile(@PathVariable(value = "id") Long fileId,
                                     Principal principal) {
        modelService.deleteFile((UUID) ((UsernamePasswordAuthenticationToken) principal).getPrincipal(), fileId);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
