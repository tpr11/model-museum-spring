package com.tpr.modelmuseum.exceptions;

import io.jsonwebtoken.lang.Collections;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.ArrayList;
import java.util.List;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class InvalidDataException extends RuntimeException {

    private List<String> errors;

    public InvalidDataException(String message) {
        super();
        this.errors = new ArrayList<>();
        this.errors.add(message);
    }

    public InvalidDataException(List<String> errors) {
        this.errors = errors;
    }

    public List<String> getErrors() {
        return errors;
    }
}
