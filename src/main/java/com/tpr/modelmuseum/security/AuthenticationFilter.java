package com.tpr.modelmuseum.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tpr.modelmuseum.model.entities.User;
import com.tpr.modelmuseum.services.UserService;
import com.tpr.modelmuseum.util.Config;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.FilterChain;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

public class AuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    @Autowired
    private UserService userService;

    private AuthenticationManager authenticationManager;

    AuthenticationFilter(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    /**
     * An Authentication object contains a Principal attribute and a Credentials attribute.
     * The credentials attribute should signify the user's password.
     * The principal can be any identifying information; for example, the username, or the email.
     *
     */
    @Override
    public Authentication attemptAuthentication(HttpServletRequest req,
                                                HttpServletResponse res) throws AuthenticationException {
        try {
            User user = new ObjectMapper().readValue(req.getInputStream(), User.class);
            return authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                    user.getUsername(),
                    user.getPassword(),
                    new ArrayList<>()
            ));
        }
        catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
    }

    /**
     * On successful authentications, a JSON web token is generated and returned to the caller within the "Authorization" header.
     * The JWT is generated using the String form of the user's id (type: UUID).
     * Since an Authentication object does not contain the user's id (the user is not aware of their id when logging in), a service call is required to find it using the Authentication's Principal object (in this case, the username).
     */
    @Override
    public void successfulAuthentication(HttpServletRequest req,
                                         HttpServletResponse res,
                                         FilterChain chain,
                                         Authentication auth) throws IOException, ServletException {
        ServletContext servletContext = req.getServletContext();
        WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
        userService = webApplicationContext.getBean(UserService.class);
        String username = ((org.springframework.security.core.userdetails.User) auth.getPrincipal()).getUsername();
        User user = userService.getUser(username);
        String token = Jwts
                .builder()
                .setSubject(user.getId().toString())
                .setExpiration(new Date(System.currentTimeMillis() + Config.JWT_LIFETIME))
                .signWith(SignatureAlgorithm.HS512, Config.JWT_SECRET)
                .compact();
        res.addHeader("Authorization", "Bearer " + token);
        res.addHeader("Access-Control-Expose-Headers", "Authorization");
        res.setStatus(204);
    }

}
