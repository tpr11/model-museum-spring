package com.tpr.modelmuseum.security;

import com.tpr.modelmuseum.exceptions.ForbiddenException;
import com.tpr.modelmuseum.exceptions.InvalidDataException;
import com.tpr.modelmuseum.exceptions.NotFoundException;
import org.json.JSONArray;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.nio.file.AccessDeniedException;

@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleNoHandlerFoundException(NoHandlerFoundException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        ex.printStackTrace();
        return new ResponseEntity<>("Not found.", HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = ForbiddenException.class)
    protected ResponseEntity<String> handleForbidden(ForbiddenException ex, WebRequest request) {
        ex.printStackTrace();
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(value = NotFoundException.class)
    protected ResponseEntity<String> handleNotFound(NotFoundException ex, WebRequest request) {
        ex.printStackTrace();
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = InvalidDataException.class)
    protected ResponseEntity<JSONArray> handleInvalidData(InvalidDataException ex, WebRequest request) {
        ex.printStackTrace();
        return new ResponseEntity<>(new JSONArray(ex.getErrors()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = AccessDeniedException.class)
    protected ResponseEntity<String> handleAccessDenied(RuntimeException ex, WebRequest request) {
        ex.printStackTrace();
        return new ResponseEntity<>("Access denied.", HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(value = Exception.class)
    protected ResponseEntity<String> handleConflict(Exception ex, WebRequest request) {
        ex.printStackTrace();
        return new ResponseEntity<>("", HttpStatus.INTERNAL_SERVER_ERROR);
    }
}