package com.tpr.modelmuseum.security;

import com.tpr.modelmuseum.util.Config;
import io.jsonwebtoken.Jwts;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.UUID;

public class AuthorizationFilter extends BasicAuthenticationFilter {

    AuthorizationFilter(AuthenticationManager authenticationManager) {
        super(authenticationManager);
    }

    /**
     * Filters unathorized (no Authorization) attempts and assigns the UsernamePasswordAuthenticationToken (containing the user's id).
     */
    @Override
    protected void doFilterInternal(HttpServletRequest req,
                                    HttpServletResponse res,
                                    FilterChain chain) throws IOException, ServletException {
        String header = req.getHeader("Authorization");
        if (header == null || !header.startsWith("Bearer ")) {
            chain.doFilter(req, res);
            return;
        }

        UsernamePasswordAuthenticationToken authentication = getAuthentication(req);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        chain.doFilter(req, res);
    }

    /**
     * Helper method that extracts the userId from the JWT and stores it within a UsernamePasswordAuthenticationToken for further use within the application.
     * Whenever the requesting user is required by a controller, the Principal object (a UUID signifying the user's id) can be injected as a function parameter to the controller.
     * Note that credentials are not stored.
     */
    private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request) {
        String token = request.getHeader("Authorization");
        if (token != null) {
            UUID userId = UUID.fromString(Jwts
                    .parser()
                    .setSigningKey(Config.JWT_SECRET)
                    .parseClaimsJws(token.replace("Bearer ", ""))
                    .getBody()
                    .getSubject());
            return new UsernamePasswordAuthenticationToken(userId, null, new ArrayList<>());
        }
        return null;
    }

}
