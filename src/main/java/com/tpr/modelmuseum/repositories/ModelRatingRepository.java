package com.tpr.modelmuseum.repositories;

import com.tpr.modelmuseum.model.entities.Model;
import com.tpr.modelmuseum.model.entities.ModelRating;
import com.tpr.modelmuseum.model.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface ModelRatingRepository extends JpaRepository<ModelRating, Long> {
    Optional<ModelRating> findByUserIdAndModelId(UUID userId, Long modelId);
}
