package com.tpr.modelmuseum.repositories;

import com.tpr.modelmuseum.model.entities.LocalFile;

public interface LocalFileRepository extends LocalFileBaseRepository<LocalFile> {
}
