package com.tpr.modelmuseum.repositories;

import com.tpr.modelmuseum.model.entities.MediaFile;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface MediaFileRepository extends LocalFileBaseRepository<MediaFile> {

    @Query(value = "SELECT index " +
            "FROM models_media_files " +
            "WHERE media_files_id = :id",
            nativeQuery = true)
    Integer getAssociativeTableIndex(@Param("id") Long id);

    @Modifying
    @Query(value = "DELETE FROM models_media_files " +
            "WHERE media_files_id = :id",
            nativeQuery = true)
    void deleteAssociativeTableKey(@Param("id") Long id);

    @Modifying
    @Query(value = "UPDATE models_media_files " +
            "SET index = index - 1 " +
            "WHERE model_id = ( " +
            "   SELECT model_id" +
            "   FROM local_files " +
            "   WHERE id = :id) " +
            "AND index > :index ",
            nativeQuery = true)
    void updateAssociativeTableIndexes(@Param("id") Long id, @Param("index") Integer index);

    @Modifying
    @Query(value = "DELETE FROM media_files " +
            "WHERE id = :id",
            nativeQuery = true)
    void deleteMediaFileKey(@Param("id") Long id);

    @Modifying
    @Query(value = "DELETE FROM local_files " +
            "WHERE id = :id",
            nativeQuery = true)
    void deleteLocalFilesKey(@Param("id") Long id);
}
