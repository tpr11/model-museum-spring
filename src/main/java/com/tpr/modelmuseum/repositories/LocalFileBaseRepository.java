package com.tpr.modelmuseum.repositories;

import com.tpr.modelmuseum.model.entities.LocalFile;
import com.tpr.modelmuseum.model.entities.Model;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@NoRepositoryBean
@Repository
public interface LocalFileBaseRepository<T extends LocalFile> extends JpaRepository<LocalFile, Long>, PagingAndSortingRepository<LocalFile, Long> {
    void deleteByIdIn(List<Long> fileIds);
    Set<Model> findModelByIdIn(List<Long> modelId);
}
