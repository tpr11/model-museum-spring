package com.tpr.modelmuseum.repositories;

import com.tpr.modelmuseum.model.ModelType;
import com.tpr.modelmuseum.model.entities.Model;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ModelRepository extends PagingAndSortingRepository<Model, Long> {

    @Query(value = "SELECT new Model(m, COALESCE(AVG(mr.rating), 0) AS averageRating, COUNT(mr.rating) AS ratings) " +
            "FROM Model m " +
            "LEFT JOIN ModelRating mr ON m.id = mr.model " +
            "GROUP BY m.id",
            countQuery = "SELECT COUNT(id) FROM Model")
    Page<Model> findAllWithRatings(Pageable pageable);

    Page<Model> findAllByModelType(Pageable pageable, ModelType modelType);
    @Query(value = "SELECT new Model(m, COALESCE(AVG(mr.rating), 0) AS averageRating, COUNT(mr.rating) AS ratings) " +
            "FROM Model m " +
            "LEFT JOIN ModelRating mr ON m.id = mr.model " +
            "WHERE m.modelType = :type " +
            "GROUP BY m.id",
            countQuery = "SELECT COUNT(id) FROM Model WHERE modelType = :type")
    Page<Model> findAllByModelTypeWithRatings(Pageable pageable, @Param("type") ModelType modelType);
}
